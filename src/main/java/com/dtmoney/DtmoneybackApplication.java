package com.dtmoney;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DtmoneybackApplication {

	public static void main(String[] args) {
		SpringApplication.run(DtmoneybackApplication.class, args);
	}

}

package com.dtmoney.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dtmoney.model.Transactions;
import com.dtmoney.repository.TransactionsRepository;

@RestController
@RequestMapping("transactions")
public class TransactionsController {

	@Autowired
	private TransactionsRepository repository;
	
//	@GetMapping
//	public Page<String> transactionsList(@Param(value = "category") String category, Pageable pageable){
//		return repository.findByCategory(category, pageable);
//		
//	}
	
	
	@GetMapping
	public Page<Transactions> findList(Pageable pageable){
		return repository.findAll(pageable);
		
	    
	}
//	@PostMapping
//	public ResponseEntity<?> insertTransactions(@RequestBody Transactions t){
//		try {
//			t.setCreated_at(new Date());
//			repository.save(t);
//			return ResponseEntity.status(HttpStatus.OK).body("salvo com sucesso");
//		} catch (Exception e) {
//			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Erro" + e.getMessage());
//		}
//	}
}

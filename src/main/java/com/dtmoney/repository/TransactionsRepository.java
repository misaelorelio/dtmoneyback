package com.dtmoney.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dtmoney.model.Transactions;

@Repository
public interface TransactionsRepository extends JpaRepository<Transactions, Integer>{
	
//	@Query(value = "select * from transactions where category = ?#{category}", nativeQuery = true)
//	public Page<String> findByCategory(@Param(value = "") String category, Pageable pageable);
	
	
	//List<Transactions> findList( String category);
}
